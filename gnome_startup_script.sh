#!/bin/bash -x


##Check in which Os is executing and use DNF or YUM
if [ $(grep -c "Maipo" /etc/os-release) -eq 1 ]
then
	OS="yum"
else
	OS="dnf"
fi


##Update the system and install vim
$OS -y update
$OS -y install vim


##Install rpm fusion
if [ $OS == "dnf" ]
then
	$OS -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm 
	$OS -y update
else
	$OS -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
	$OS -y install https://download1.rpmfusion.org/free/el/rpmfusion-free-release-7.noarch.rpm
	$OS -y install https://download1.rpmfusion.org/nonfree/el/rpmfusion-nonfree-release-7.noarch.rpm
	$OS -y update
fi


##Install multimedia Codecs
$OS -y install gstreamer1-libav gstreamer1-plugins-bad-free-extras gstreamer1-plugins-bad-freeworld gstreamer1-plugins-good-extras gstreamer1-plugins-ugly gstreamer-ffmpeg xine-lib-extras xine-lib-extras-freeworld gstreamer-plugins-bad gstreamer-plugins-bad-free-extras gstreamer-plugins-bad-nonfree gstreamer-plugins-ugly gstreamer-ffmpeg mencoder


#Install i3 desktop minimal tools
$OS -y install i3 i3status dmenu i3lock feh tmux tilda network-manager-applet arandr rofi light arandr lxappearance pasystray


##Design and Edition
$OS -y install gimp inkscape freecad 


##Audio
$OS -y install vlc


##System Administration
$OS -y install 	gparted wget curl htop tmux guake tilda 


##Comunication
$OS -y install polari transmission 


##Compression/Decompression
$OS -y install unrar unzip zip p7zip p7zip-plugins


##Printers and Scaners
$OS -y install python-qt4 hplip hplip-gui libsane-hpaio simple-scan


##Java
$OS -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel icedtea-web


##gnome utilities
$OS -y install cheese gnome-shell-extension-common dconf-editor gnome-tweak-tool gtk-murrine-engine* libreoffice-langpack-es


##Compilation Software
$OS -y install kernel-headers kernel-devel dkms
$OS -y groupinstall "Development Tools" && $OS -y groupinstall "Development Libraries"


##Chromium Browser & epiphany
$OS -y install chromium epiphany firefox


##Google Chrome 
$OS -y install https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm


##Opera Browser
$OS -y install http://download3.operacdn.com/pub/opera/desktop/49.0.2725.39/linux/opera-stable_49.0.2725.39_amd64.rpm


##Install 0ad
$OS -y install 0ad


##Spyder IDE for python
$OS -y install spyder


##Sublime Text
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo $OS config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
$OS -y install sublime-text



##Atom Editor
##$OS -y install https://atom-installer.github.com/v1.22.1/atom.x86_64.rpm?s=1510769298&ext=.rpm


##Virtual Machine Manager
$OS -y install @Virtualization


#Zsh
$OS -y install zsh
#chsh -s /bin/zsh
#sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"


#Install fontAwesome
$OS -y install fontawesome-fonts


#Adding pycharm
$OS -y copr enable phracek/PyCharm 
$OS -y install pycharm-community


#Speetest-cli
$OS -y install speedtest-cli


#Blueman Administration for bluetooth
$OS -y install blueman


#Tor Browser
$OS -y install torbrowser-launcher


#Synergy
$OS -y install synergy


#Tor Browser
$OS -y install torbrowser-launcher


#Cool retro term
$OS -y install cool-retro-term


#Adding OTPClient
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub com.github.paolostivanin.OTPClient

#Last update
$OS -y update
