#!/bin/bash -x 
echo "Startup script for PureOS"

#Installing
echo "Installing Software"

apt -y install mencoder
apt -y install i3 i3status suckless-tools i3lock feh tmux tilda arandr rofi  arandr lxappearance pasystray
apt -y install gimp inkscape freecad
apt -y install vlc
apt -y install gparted wget curl htop tmux guake tilda
apt -y install polari transmission
apt -y install python-qt4 hplip hplip-gui libsane-hpaio simple-scan
apt -y install chromium tor
apt -y install zsh speedtest-cli

echo "Installing process finished"
